<?php 
require_once 'vendor/autoload.php';

use Libs\Imagen\ImageThumb;
use Libs\Matematica\Financiera;
use Libs\Seguridad\Encriptacion;
use Carbon\Carbon;

$imagen = new ImageThumb();

$matematica = new Financiera();

$seguridad = new Encriptacion();

printf("Ahora: %s", Carbon::now());
echo PHP_EOL;

printf("La hora en Mexico es %s", Carbon::now('America/Mexico_City'));

echo PHP_EOL;
echo PHP_EOL;

echo '<br/>';
echo "----------------------------------------------------------------------------------------------";
echo PHP_EOL;
echo '<br/>';


printf("Right now is %s", Carbon::now()->toDateTimeString());
echo PHP_EOL;
echo PHP_EOL;

printf("Right now in Vancouver is %s", Carbon::now('America/Mexico_City'));  //implicit __toString()
echo PHP_EOL;
echo PHP_EOL;
echo '<br/>';

$tomorrow = Carbon::now()->addDay();
echo "Mañana ".$tomorrow.PHP_EOL;
echo '<br/>';

$lastWeek = Carbon::now()->subWeek();
echo "Semana Anterior ".$lastWeek.PHP_EOL;
echo '<br/>';

$nextSummerOlympics = Carbon::createFromDate(2016)->addYears(4);
echo "Siguientes Juegos Olimpicos ".$nextSummerOlympics.PHP_EOL;
echo '<br/>';

$officialDate = Carbon::now()->toRfc2822String();
echo "Fecha Oficial ". PHP_EOL;
echo '<br/>';

$howOldAmI = Carbon::createFromDate(1982, 1, 10)->age;
echo "Mi edad es ". $howOldAmI . PHP_EOL;
echo '<br/>';

$noonTodayLondonTime = Carbon::createFromTime(12, 0, 0, 'America/Mexico_City');

$internetWillBlowUpOn = Carbon::create(2038, 01, 19, 3, 14, 7, 'GMT');

// Don't really want this to happen so mock now
Carbon::setTestNow(Carbon::createFromDate(2000, 1, 1));

// comparisons are always done in UTC
if (Carbon::now()->gte($internetWillBlowUpOn)) {
    die();
}

// Phew! Return to normal behaviour
Carbon::setTestNow();

if (Carbon::now()->isWeekend()) {
    echo 'Party!';
}
echo Carbon::now()->subMinutes(2)->diffForHumans(); // '2 minutes ago'

echo '<br/>';
echo PHP_EOL;
echo "----------------------------------------------------------------------------------------------";
echo PHP_EOL;
echo '<br/>';

setlocale(LC_TIME, 'Spanish');
$dt = Carbon::create(2016, 01, 06, 00, 00, 00);
Carbon::setUtf8(false);
echo $dt->formatLocalized('%A %d %B %Y'). PHP_EOL;          // mi�rcoles 06 enero 2016
echo '<br/>';

Carbon::setUtf8(true);
echo $dt->formatLocalized('%A %d %B %Y'). PHP_EOL;          // miércoles 06 enero 2016
echo '<br/>';

Carbon::setUtf8(false);
setlocale(LC_TIME, '');